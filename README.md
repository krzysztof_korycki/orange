# orange

1) Grupa - aplikacja Vue JS 
* Zbudowanie obrazu dockerowego
* Wdrożenie na platformę Kubernetes
* Udostępnienie aplikacji poprzez NodePort
https://github.com/gothinkster/vue-realworld-example-app

2) Grupa - wdrożenie ElasticSearch
* Zbudowanie obrazu dockerowego z Stempel Polish Analysis
* 1 master, 2 slave
* Zbudowanie LoadBalancera dla ElasticSearch

3) Grupa - aplikacja Rev Proxy
* Użycie Varnisha
* Użycie nginxa
* https://github.com/cosmicjs/static-website

4) Grupa - Jenkins X
5) Grupa - Calico
